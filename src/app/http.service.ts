import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'



@Injectable({
  providedIn: 'root'
})
export class HttpService {

  //private _url: String = "https://api.openbrewerydb.org/breweries";
  private _url:any = "/assets/data/brewery.json";

  constructor(private http: HttpClient) { }

  getBeer(): Observable<any>{
    return this.http.get<any>(this._url)
    .pipe( catchError( this.handleError) )
  }

  handleError(err){
    console.log("Inside global sevice error handler", err);
    return throwError(err);
  }
}
