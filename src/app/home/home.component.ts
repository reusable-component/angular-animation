import { Component, OnInit } from '@angular/core';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
},
animations: [
flyInOut(),
expand()
]
})
export class HomeComponent implements OnInit {

 clickCounter: number = 0;
 name: string = '';
 public email = 'kuber.gaur@capgemini.com';

  constructor() { }

  ngOnInit(): void {
  }

  countClick(){
    this.clickCounter += 1;
  }

  resetClick(){
    this.clickCounter -= this.clickCounter;
  }

  setClasses(){
    let activeClass = {
      active : this.clickCounter > 4,
      noactive: this.clickCounter <= 4
    }

    return activeClass;
  }

}
