import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';

import { visibility, flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  host: {
        '[@flyInOut]': 'true',
        'style': 'display: block;'
  },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class ListComponent implements OnInit {

  beers: Array<any> = [];
  errorMessage: any;
  visibility = 'shown';
  
  constructor( private _http : HttpService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this._http.getBeer().subscribe( data => {
      this.visibility = 'hidden';
      setTimeout(()=>{ 
        this.beers = data;
        this.visibility = 'shown';  }, 500)
      }, error => {
        this.errorMessage = error.message;
      }
    );
  }

}
